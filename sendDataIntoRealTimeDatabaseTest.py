import unittest
from unittest.mock import patch, MagicMock
from sendDataIntoRealTimeDatabase import get_cpu_temperature, get_cpu_load, append_system_info_to_history, get_memory_usage, get_gpu_power
from datetime import datetime
from unittest.mock import ANY

class TestFunctions(unittest.TestCase):

    @patch('sendDataIntoRealTimeDatabase.wmi')
    def test_get_cpu_temperature(self, mock_wmi):
        mock_sensor = MagicMock()
        mock_sensor.SensorType = 'Temperature'
        mock_sensor.Name = 'CPU'
        mock_sensor.Value = 50

        mock_wmi.WMI.return_value.Sensor.return_value = [mock_sensor]

        temperature = get_cpu_temperature()
        self.assertEqual(temperature, 50)

    # test
    @patch('sendDataIntoRealTimeDatabase.wmi')
    def test_get_cpu_load(self, mock_wmi):
        mock_wmi.WMI.return_value.Win32_PerfFormattedData_PerfOS_Processor.return_value = [
            MagicMock(PercentProcessorTime=70)
        ]

        cpu_load = get_cpu_load()
        self.assertEqual(cpu_load, 70)

    @patch('sendDataIntoRealTimeDatabase.db.reference')
    def append_system_info_to_history(self, mock_reference):
        # Mock the return value of the reference.get() method
        mock_reference.return_value.get.return_value = [{'timestamp': '2024-02-29 15:00:00', 'temperature': 50, 'load': 70}]

        # Define the expected call with the updated timestamp value
        expected_call = [{'timestamp': '2024-02-29 15:00:00', 'temperature': 50, 'load': 70},
                        {'timestamp': '2024-03-01 09:52:30', 'temperature': 60, 'load': 80}]

        # Assert that the set method was called with the expected parameters
        mock_reference.return_value.set.assert_called_once_with(expected_call)

    @patch('sendDataIntoRealTimeDatabase.wmi')
    def test_get_memory_usage(self, mock_wmi):
        # Mocking the WMI response for memory usage
        mock_memory = MagicMock()
        mock_memory.TotalVisibleMemorySize = 1024 * 1024 * 1024  # 1GB
        mock_memory.FreePhysicalMemory = 512 * 1024 * 1024  # 512MB

        mock_wmi.WMI.return_value.Win32_OperatingSystem.return_value = [mock_memory]

        # Expected memory usage: (1GB - 512MB) / 1GB * 100 = 50%
        memory_usage = get_memory_usage()
        self.assertEqual(memory_usage, 50)

    @patch('sendDataIntoRealTimeDatabase.wmi')
    def test_get_gpu_power(self, mock_wmi):
        # Mocking the WMI response for GPU power
        mock_sensor = MagicMock()
        mock_sensor.SensorType = 'Power'
        mock_sensor.Name = 'GPU'
        mock_sensor.Value = 100  # Mock GPU power value

        mock_wmi.WMI.return_value.Sensor.return_value = [mock_sensor]

        gpu_power = get_gpu_power()
        self.assertEqual(gpu_power, 100)

    @patch('sendDataIntoRealTimeDatabase.get_cpu_temperature')
    @patch('sendDataIntoRealTimeDatabase.get_cpu_load')
    @patch('sendDataIntoRealTimeDatabase.get_memory_usage')
    @patch('sendDataIntoRealTimeDatabase.get_gpu_power')
    @patch('sendDataIntoRealTimeDatabase.db.reference')
    def test_append_system_info_to_history(self, mock_reference, mock_gpu_power, mock_memory_usage, mock_cpu_load, mock_cpu_temperature):
        # Mocked data
        mock_reference.return_value.get.return_value = [{'timestamp': '2024-02-29 15:00:00', 'cpu_temperature': 50, 'cpu_load': 70, 'memory_usage': 80, 'gpu_power': 90}]
        mock_cpu_temperature.return_value = 60
        mock_cpu_load.return_value = 80
        mock_memory_usage.return_value = 70
        mock_gpu_power.return_value = 90
        
        # Call the method
        append_system_info_to_history()

        # Verify calls
        mock_cpu_temperature.assert_called_once()
        mock_cpu_load.assert_called_once()
        mock_memory_usage.assert_called_once()
        mock_gpu_power.assert_called_once()
        mock_reference.assert_called_once_with('/system_info_history')
        mock_reference.return_value.get.assert_called_once()

        # Verify set method called with expected data
        mock_reference.return_value.set.assert_called()

if __name__ == '__main__':
    unittest.main()