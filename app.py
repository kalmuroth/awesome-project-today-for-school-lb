import streamlit as st
from firebase_admin import credentials, db
from datetime import datetime
import matplotlib.pyplot as plt
import firebase_admin
import time

if not firebase_admin._apps:
    cred = credentials.Certificate(r"C:\Users\user\awesome-project-today-for-school-lb\devsecopslbprojet-firebase-adminsdk-zhm7r-1edbdd072c.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://devsecopslbprojet-default-rtdb.europe-west1.firebasedatabase.app'
    })

# Fetch data from Firebase
def fetch_data():
    try:
        ref = db.reference('/system_info_history')
        history = ref.get()
        if history is None:
            return [], [], [], [], []

        timestamps = []
        temperatures = []
        loads = []
        memories = []
        powers = []

        last_minute = None

        for entry in history:
            timestamp = entry.get("timestamp")
            current_minute = timestamp.split()[1].split(':')[1]

            if current_minute != last_minute:
                timestamps.append(timestamp)
                temperatures.append(entry.get("cpu_temperature", None))
                loads.append(entry.get("cpu_load", None))
                memories.append(entry.get("memory_usage", None))
                powers.append(entry.get("gpu_power", None))
                last_minute = current_minute

        sorted_indices = sorted(range(len(powers)), key=lambda i: powers[i])
        timestamps = [timestamps[i] for i in sorted_indices]
        temperatures = [temperatures[i] for i in sorted_indices]
        loads = [loads[i] for i in sorted_indices]
        memories = [memories[i] for i in sorted_indices]
        powers = [powers[i] for i in sorted_indices]

        return timestamps, temperatures, loads, memories, powers

    except Exception as e:
        print("Error fetching data:", e)
        return [], [], [], [], []


def format_timestamps(timestamps):
    return [datetime.strptime(ts, "%Y-%m-%d %H:%M:%S").strftime("%Hh%M") for ts in timestamps]

def main():
    st.title('System Data History')

    graph_placeholder = st.empty()

    display_type = st.selectbox("Select data to display:", options=["CPU Temperature", "CPU Load", "Memory Usage", "GPU Power"], format_func=lambda x: x)

    while True:
        timestamps, temperatures, loads, memories, powers = fetch_data()

        if timestamps and ((display_type == "CPU Temperature" and temperatures) or
                           (display_type == "CPU Load" and loads) or
                           (display_type == "Memory Usage" and memories) or
                           (display_type == "GPU Power" and powers)):
            plt.figure(figsize=(10, 6))
            if display_type == "CPU Temperature":
                plt.plot(format_timestamps(timestamps), temperatures, marker='o', linestyle='-', color='blue', label='CPU Temperature')
                plt.ylabel('CPU Temperature')
                plt.title('CPU Temperature over Time')
            elif display_type == "CPU Load":
                plt.plot(format_timestamps(timestamps), loads, marker='o', linestyle='-', color='orange', label='CPU Load')
                plt.ylabel('CPU Load')
                plt.title('CPU Load over Time')
            elif display_type == "Memory Usage":
                plt.plot(format_timestamps(timestamps), memories, marker='o', linestyle='-', color='green', label='Memory Usage')
                plt.ylabel('Memory Usage')
                plt.title('Memory Usage over Time')
            elif display_type == "GPU Power":
                plt.plot(format_timestamps(timestamps), powers, marker='o', linestyle='-', color='red', label='GPU Power')
                plt.ylabel('GPU Power')
                plt.title('GPU Power over Time')
            plt.xlabel('Time')
            
            plt.xticks(range(0, len(timestamps), max(1, len(timestamps)//10)), [format_timestamps(timestamps)[i] for i in range(0, len(timestamps), max(1, len(timestamps)//10))], rotation=45)
            
            graph_placeholder.pyplot(plt)
        elif not timestamps:
            st.write("No data available to display.")
        else:
            st.write(f"No {display_type.lower()} data available to display.")

        time.sleep(5)

if __name__ == "__main__":
    main()
