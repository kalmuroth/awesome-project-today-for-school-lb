import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time
import wmi
from datetime import datetime

cred = credentials.Certificate(r"C:\Users\user\awesome-project-today-for-school-lb\devsecopslbprojet-firebase-adminsdk-zhm7r-1edbdd072c.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://devsecopslbprojet-default-rtdb.europe-west1.firebasedatabase.app'
})

# Function to get CPU temperature on Windows
def get_cpu_temperature():
    try:
        w = wmi.WMI(namespace="root/OpenHardwareMonitor")
        temperature_infos = w.Sensor()
        for sensor in temperature_infos:
            if sensor.SensorType == 'Temperature' and 'cpu' in sensor.Name.lower():
                return sensor.Value
    except Exception as e:
        print("Failed to get CPU temperature:", e)
        return None

# Function to get CPU load on Windows
def get_cpu_load():
    try:
        w = wmi.WMI()
        cpu_load = w.Win32_PerfFormattedData_PerfOS_Processor()[0].PercentProcessorTime
        return cpu_load
    except Exception as e:
        print("Failed to get CPU load:", e)
        return None

def append_temperature_to_history(temperature, load):
    try:
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        data = {"timestamp": timestamp, "temperature": temperature, "load": load}
        
        ref = db.reference('/cpu_info_history')
        history = ref.get()
        if history is None:
            history = []
        history.append(data)
        ref.set(history)
        print("CPU temperature/load added to history in Firebase:", data)
    except Exception as e:
        print("Failed to append temperature/load to history in Firebase:", e)

# Send data to Firebase
def send_to_firebase():
    while True:
        cpu_temperature = get_cpu_temperature()
        cpu_load = get_cpu_load()
        if cpu_temperature is not None:
            append_temperature_to_history(cpu_temperature, cpu_load)
        time.sleep(60)

if __name__ == "__main__":
    send_to_firebase()