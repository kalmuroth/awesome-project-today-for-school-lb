import wmi
import time
from datetime import datetime
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

cred = credentials.Certificate(r"C:\Users\user\awesome-project-today-for-school-lb\devsecopslbprojet-firebase-adminsdk-zhm7r-1edbdd072c.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://devsecopslbprojet-default-rtdb.europe-west1.firebasedatabase.app'
})

def get_cpu_temperature():
    try:
        w = wmi.WMI(namespace="root/OpenHardwareMonitor")
        temperature_infos = w.Sensor()
        for sensor in temperature_infos:
            if sensor.SensorType == 'Temperature' and 'cpu' in sensor.Name.lower():
                return sensor.Value
    except Exception as e:
        print("Failed to get CPU temperature:", e)
        return None

def get_cpu_load():
    try:
        w = wmi.WMI()
        cpu_load = w.Win32_PerfFormattedData_PerfOS_Processor()[0].PercentProcessorTime
        return cpu_load
    except Exception as e:
        print("Failed to get CPU load:", e)
        return None

def get_memory_usage():
    try:
        w = wmi.WMI()
        memory = w.Win32_OperatingSystem()[0]
        total_memory = int(memory.TotalVisibleMemorySize)
        free_memory = int(memory.FreePhysicalMemory)
        used_memory = total_memory - free_memory
        memory_usage = (used_memory / total_memory) * 100
        return memory_usage
    except Exception as e:
        print("Failed to get memory usage:", e)
        return None

def get_gpu_power():
    try:
        w = wmi.WMI(namespace="root/OpenHardwareMonitor")
        power_infos = w.Sensor()
        for sensor in power_infos:
            if sensor.SensorType == 'Power' and 'gpu' in sensor.Name.lower():
                return sensor.Value
    except Exception as e:
        print("Failed to get GPU power:", e)
        return None

def append_system_info_to_history():
    try:
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        cpu_temperature = get_cpu_temperature()
        cpu_load = get_cpu_load()
        memory_usage = get_memory_usage()
        gpu_power = get_gpu_power()

        data = {
            "timestamp": timestamp,
            "cpu_temperature": cpu_temperature,
            "cpu_load": cpu_load,
            "memory_usage": memory_usage,
            "gpu_power": gpu_power
        }

        ref = db.reference('/system_info_history')
        history = ref.get()
        if history is None:
            history = []
        history.append(data)
        ref.set(history)
        print("System information added to history in Firebase:", data)
    except Exception as e:
        print("Failed to append system information to history in Firebase:", e)

def send_system_info_to_firebase():
    while True:
        append_system_info_to_history()
        time.sleep(5)

if __name__ == "__main__":
    send_system_info_to_firebase()
