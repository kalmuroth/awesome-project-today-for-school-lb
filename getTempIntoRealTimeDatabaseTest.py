import unittest
from unittest.mock import patch, MagicMock
from getTempIntoRealTimeDatabase import get_cpu_temperature, get_cpu_load, append_temperature_to_history
from datetime import datetime

class TestFunctions(unittest.TestCase):

    @patch('getTempIntoRealTimeDatabase.wmi')
    def test_get_cpu_temperature(self, mock_wmi):
        mock_sensor = MagicMock()
        mock_sensor.SensorType = 'Temperature'
        mock_sensor.Name = 'CPU'
        mock_sensor.Value = 50

        mock_wmi.WMI.return_value.Sensor.return_value = [mock_sensor]

        temperature = get_cpu_temperature()
        self.assertEqual(temperature, 50)

    @patch('getTempIntoRealTimeDatabase.wmi')
    def test_get_cpu_load(self, mock_wmi):
        mock_wmi.WMI.return_value.Win32_PerfFormattedData_PerfOS_Processor.return_value = [
            MagicMock(PercentProcessorTime=70)
        ]

        cpu_load = get_cpu_load()
        self.assertEqual(cpu_load, 70)

    @patch('getTempIntoRealTimeDatabase.db.reference')
    def test_append_temperature_to_history(self, mock_reference):
        mock_reference.return_value.get.return_value = [{'timestamp': '2024-02-29 15:00:00', 'temperature': 50, 'load': 70}]

        current_timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        append_temperature_to_history(60, 80)

        expected_call = [{'timestamp': '2024-02-29 15:00:00', 'temperature': 50, 'load': 70},
                         {'timestamp': current_timestamp, 'temperature': 60, 'load': 80}]

        mock_reference.return_value.set.assert_called_once_with(expected_call)

if __name__ == '__main__':
    unittest.main()
